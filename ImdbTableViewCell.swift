//
//  ImdbTableViewCell.swift
//  IMDB
//
//  Created by ahmed abbas on 8/31/16.
//  Copyright © 2016 Blink22. All rights reserved.
//

import UIKit

class ImdbTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var imageLabel: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
