//
//  ImdbData.swift
//  IMDB
//
//  Created by ahmed abbas on 8/31/16.
//  Copyright © 2016 Blink22. All rights reserved.
//

import Foundation
import CoreData

class ServerManager {
    
    static func fetchMoviesNSData(urlString: String, completionHandler: (AnyObject?) -> Void ) {
        let url = NSURL(string: urlString)
        let request = NSURLRequest(URL: url!)
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
            var movies: AnyObject?
            if(error == nil) {
                let jsonDic = self.convertStringToDictionary(NSString(data: data!, encoding: NSUTF8StringEncoding)! as String)!
                movies = jsonDic["results"]!
            }
            completionHandler(movies)
        })
        task.resume()
    }

    
    static func fetchImagesNSData(urlString: String, completionHandler: (NSData?) -> Void ) {
        let url = NSURL(string: urlString)
        let request = NSURLRequest(URL: url!)
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
            completionHandler(data)
        })
        task.resume()
 
    }
    
    private static func convertStringToDictionary(text: String) -> [String: AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
}
