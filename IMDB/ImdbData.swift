//
//  ImdbData.swift
//  IMDB
//
//  Created by ahmed abbas on 8/31/16.
//  Copyright © 2016 Blink22. All rights reserved.
//

import Foundation

class ImdbData {
    
    var original_title: String
    var vote_average: Double
    var imageData: NSData
    var imageUrl: String
    init(original_title: String, vote_average: Double, imageData: NSData, imageUrl: String) {
        self.original_title = original_title
        self.vote_average = vote_average
        self.imageData = imageData
        self.imageUrl = imageUrl
    }
}
