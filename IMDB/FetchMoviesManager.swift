//
//  ImdbData.swift
//  IMDB
//
//  Created by ahmed abbas on 8/31/16.
//  Copyright © 2016 Blink22. All rights reserved.
//

import Foundation
import CoreData
import UIKit


class FetchMoviesManager {
    
    var urlStr: String
    var entityName: String
    var imdbData: [ImdbData]
    
    init(uslStr: String, entityName: String) {
        self.urlStr = uslStr
        self.imdbData = []
        self.entityName = entityName
    }
    
    func loadMovies(completionHandler: (AnyObject?) -> Void) {
        ServerManager.fetchMoviesNSData(self.urlStr, completionHandler: {(movies) in
            self.loadImagesData(movies!, completionHandler: {(imageNSData) in
                if (movies != nil) {
                    for movieIdx in 0..<movies!.count {
                        let curMovie = movies![movieIdx]
                        let curMovieTitle = curMovie!["original_title"] as! String
                        let curMovieVoteAvg = curMovie!["vote_average"] as! Double
                        let curMovieVPosterPath = curMovie["poster_path"] as! String
                        
                        let curMoviePoster = imageNSData[movieIdx]
                        
                        let queryResult = CoreDataManager.fetchMovie(curMovieTitle, entityName: self.entityName)
                        
                        if (queryResult.count == 0) {
                            CoreDataManager.saveMovie(curMovieTitle, voteAverage: curMovieVoteAvg, poster: curMoviePoster!, posterUrl: curMovieVPosterPath, entityName: self.entityName)
                        }else {
                            CoreDataManager.updateEntry(curMovieTitle, newAverageRate: curMovieVoteAvg, poster: curMoviePoster!, posterUrl: curMovieVPosterPath, entityName: self.entityName)
                        }
                    }
                }

                let fetchedMovies = CoreDataManager.fetchAllMovies(self.entityName)
                
                for movie in fetchedMovies {
                    let curMovieTitle = movie.valueForKey("original_title") as! String
                    let curMovieVoteAvg = movie.valueForKey("vote_average") as! Double
                    let curMovieImageNSData = movie.valueForKey("poster") as! NSData
                    let curMoviePosterPath = movie.valueForKey("poster_url") as! String
                    
                    self.imdbData.append(ImdbData(original_title: curMovieTitle, vote_average: curMovieVoteAvg, imageData: curMovieImageNSData, imageUrl: curMoviePosterPath))
                }
                completionHandler([])
            })
        })
    }
    
    private func loadImagesData(movies: AnyObject, completionHandler: ([NSData?]) -> Void) {
        let myGroup = dispatch_group_create()
        var allMoviesPosters = [NSData?](count: movies.count, repeatedValue: nil)

        for movieIdx in 0..<movies.count {
            dispatch_group_enter(myGroup)
            let curMovie = movies[movieIdx]
            let moviePosterPath = curMovie["poster_path"] as! String
            let moviePosterUrl = Constants.IMAGES_PREFIX_URL + moviePosterPath
            
            ServerManager.fetchImagesNSData(moviePosterUrl, completionHandler: {(currentImageNSData) in
                allMoviesPosters[movieIdx] = currentImageNSData!
                dispatch_group_leave(myGroup)
            })
        }
        dispatch_group_notify(myGroup, dispatch_get_main_queue(), {
           completionHandler(allMoviesPosters)
        })
        
    }
    
    
    
}
