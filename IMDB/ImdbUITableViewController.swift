//
//  ImdbUITableViewController.swift
//  IMDB
//
//  Created by ahmed abbas on 8/31/16.
//  Copyright © 2016 Blink22. All rights reserved.
//

import UIKit
import CoreData
import Haneke

class ImdbUITableViewController: UITableViewController {
    
    let entityName = "Movie"
    var imdbData = [ImdbData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadSampleMovies()
    }
    
    private func loadSampleMovies() {
        let fetchMoviesManager = FetchMoviesManager(uslStr: Constants.POPULAR_MOVIES_URL, entityName: entityName)
        fetchMoviesManager.loadMovies({(data) in
            self.imdbData = fetchMoviesManager.imdbData
            self.updateTable()
        })
    }
    
    private func updateTable() {
        dispatch_async(dispatch_get_main_queue(), {
            self.tableView.reloadData()
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imdbData.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "ImdbTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! ImdbTableViewCell
        let movie = imdbData[indexPath.row]
        cell.nameLabel.text = movie.original_title
        cell.rateLabel.text = String(movie.vote_average)
//        using image in database
//        cell.imageLabel.image = UIImage(data: movie.imageData)
        let posterUrl = Constants.IMAGES_PREFIX_URL + movie.imageUrl
        let url = NSURL(string: posterUrl)
        cell.imageLabel.hnk_setImageFromURL(url!)
        return cell
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
