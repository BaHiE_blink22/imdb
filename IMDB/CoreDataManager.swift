//
//  CoreDataManager.swift
//  IMDB
//
//  Created by ahmed abbas on 9/1/16.
//  Copyright © 2016 Blink22. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataManager {
    
    static func saveMovie(originalTitle: String, voteAverage: Double, poster: NSData, posterUrl: String, entityName: String)
    {
        let appDelegate    = UIApplication.sharedApplication().delegate as? AppDelegate
        let managedContext = appDelegate!.managedObjectContext
        let entity         = NSEntityDescription.entityForName(entityName, inManagedObjectContext: managedContext)
        
        let person         = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
        
        person.setValue(originalTitle, forKey:"original_title")
        person.setValue(voteAverage, forKey:"vote_average")
        person.setValue(poster, forKey:"poster")
        person.setValue(posterUrl, forKey:"poster_url")

        do
        {
            try managedContext.save()
        }
        catch
        {
            print("There is some error.")
        }
    }

    static func fetchAllMovies(entityName: String) -> [NSManagedObject]
    {
        let appDelegate    = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest   = NSFetchRequest(entityName: entityName)
        var results:[NSManagedObject]?
        do
        {
            let fetchedResult = try managedContext.executeFetchRequest(fetchRequest) as? [NSManagedObject]
            results = fetchedResult
        }
        catch
        {
            print("There is some error.")
        }
        return results!
    }


    static func fetchMovie(movieTitle: String, entityName: String) -> NSArray
    {
        let appDelegate    = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest   = NSFetchRequest(entityName: entityName)
        let sortDescriptor = NSSortDescriptor(key: "original_title", ascending: true)
        let sortDescriptors = [sortDescriptor]
        
        fetchRequest.sortDescriptors = sortDescriptors
        fetchRequest.predicate = NSPredicate(format: "original_title MATCHES[cd] %@", movieTitle)
        
        var results: NSArray = []
        do
        {
            results = try managedContext.executeFetchRequest(fetchRequest)
        }
        catch
        {
            print("There is some error.")
        }
        return results;
    }

    static func updateEntry(movieTitle: String, newAverageRate: Double, poster: NSData, posterUrl: String, entityName: String)
    {
        let appDelegate    = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest   = NSFetchRequest(entityName: entityName)
        let sortDescriptor = NSSortDescriptor(key: "original_title", ascending: true)
        let sortDescriptors = [sortDescriptor]
        
        fetchRequest.sortDescriptors = sortDescriptors
        fetchRequest.predicate = NSPredicate(format: "original_title MATCHES[cd] %@", movieTitle)
        
        do
        {
            let results = try managedContext.executeFetchRequest(fetchRequest) as? [NSManagedObject]
            let currentMovie = results![0]
            currentMovie.setValue(newAverageRate, forKey: "vote_average")
            currentMovie.setValue(poster, forKey:"poster")
            currentMovie.setValue(posterUrl, forKey:"poster_url")
            try managedContext.save()
        }
        catch
        {
            print("There is some error.")
        }
    }
}