//
//  Movie+CoreDataProperties.swift
//  
//
//  Created by ahmed abbas on 9/4/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Movie {

    @NSManaged var original_title: String?
    @NSManaged var poster: NSData?
    @NSManaged var vote_average: NSNumber?
    @NSManaged var poster_url: String?

}
