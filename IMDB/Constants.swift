//
//  Constants.swift
//  IMDB
//
//  Created by ahmed abbas on 9/3/16.
//  Copyright © 2016 Blink22. All rights reserved.
//

import Foundation

class Constants {
    static let POPULAR_MOVIES_URL = "https://api.themoviedb.org/3/movie/popular?api_key=7d9fb9671065ac25c45f0aa951720b20"
    static let IMAGES_PREFIX_URL = "https://image.tmdb.org/t/p/w185/"
}